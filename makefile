# Don't forget to change the path inside fakesh if you change FILE_PATH
FILE_PATH=/usr/local/share/fakesh
EXEC_FILE=/usr/local/bin/fakesh

help:
	@echo "Please use the install or uninstall target."

install:
	sudo mkdir $(FILE_PATH)
	sudo cp ./fakesh.py $(FILE_PATH)/fakesh.py
	sudo cp ./fakesh $(FILE_PATH)/fakesh
	sudo ln -s $(FILE_PATH)/fakesh $(EXEC_FILE)

uninstall:
	sudo rm -f $(EXEC_FILE)
	sudo rm -rf $(FILE_PATH)
	@echo WARNING: FakeSH may have log files at "~/.local/share/fakesh"
