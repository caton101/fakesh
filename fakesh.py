# Copyright (c) 2021, Cameron Himes
# All rights reserved.
# See LICENSE for details or go here: https://opensource.org/licenses/BSD-3-Clause

# Imports
from blessed import Terminal
import getpass
import socket
import time
import os

# Settings
PROMPT = "[" + getpass.getuser() + "@" + socket.gethostname() + " ~]$ "
EXIT_SEQ = ['', 'e', 'x', 'i', 't', '\n']
LOGS_DIR = os.path.join(os.path.expanduser("~"), ".local", "share", "fakesh")
SPECIAL_CHARS = [
    "~", "`", "!", "@", "#", "$", "%", "^", "&", "*",
    "(", ")", "_", "+", "-", "=", "{", "}", "[", "]",
    "|", "\\", ":", "\"", ";", "'", "<", ">", ",", ".",
    "/?", "\t", " "
]

# Runtime Globals
file_path = LOGS_DIR + "/log-" + str(time.time_ns()) + ".txt"
buff_seq = []
term = Terminal()


def init_log():
    """
    ensure the LOGS_DIR is valid at runtime
    """
    if not os.path.exists(LOGS_DIR):
        os.makedirs(LOGS_DIR)


def write_log(typed_char):
    """
    write a new character to the log file

    Args:
        typed_char (str): a string with a single character
    """
    with open(file_path, "a") as f:
        f.write(typed_char)


def flush_log():
    """
    start a new line in the log file
    """
    with open(file_path, "a") as f:
        f.write('\n')


def flush_buff():
    """
    clear the buffer sequence
    """
    global buff_seq, EXIT_SEQ
    buff_seq = ['' for _ in range(len(EXIT_SEQ))]


def process_exit(typed_char):
    """
    add new character to the buffer and check if it matches the exit sequence

    Args:
        typed_char (str): a string with a single character
    """
    buff_seq.append(typed_char)
    buff_seq.pop(0)
    if buff_seq == EXIT_SEQ:
        exit()


def shell():
    """
    run the main shell program
    """
    init_log()
    flush_buff()
    while True:
        print(PROMPT, end='', flush=True)
        while True:
            typed_char = term.getch()
            process_exit(typed_char)
            if typed_char.isalpha() or typed_char.isdigit() or typed_char in SPECIAL_CHARS:
                write_log(typed_char)
            if typed_char == "\n":
                flush_buff()
                flush_log()
                break


if __name__ == "__main__":
    shell()
